Précisions:
- Tout mon CSS d'animation se trouve dans le fin fond bas de page de mon fichier CSS sous sa catégorie "Animation Tweaking" (Sauf la fenêtre popup, sous la catégorie Index-Page)

- Les boutons du menu sont déja animé en :hover avec materialize. J'ai animé mes deux boutons sur la page index "À propos" et "Contact", ainsi que sur la page "À propos" pour le bouton de téléchargement du CV.

- Mes icones SVG s'animeront à chaque fois que l'utilisateur aura scrollé jusqu'au footer. Une pierre deux coups!
(J'ai fait une animation pour mes icones sociaux dans mon footer, beaucoup plus facile à observer depuis la page "contact". J'en ai fait une autre aussi avec un SVG d'un engrenage, comme tu as pu le remarquer sur toutes les pages (sauf contact). Animation en scrolling et d'un SVG en plus. Donc une autre pierre deux coups. 

Je te réfère au lien référence de CSS-tricks, sur lequel j'ai appris à utiliser cette petite technique et à **COMPRENDRE** pour de futurs utilisations. Je pourrais vivement te l'expliquer si t'en ressens le besoin de me vérifier. Bref: https://css-tricks.com/books/greatest-css-tricks/scroll-animation/


Animation 1: 
 - Transition sur les boutons de la page index.
 - Animation scroll sur les engrenages de la page Index
 - Animation SVG sur les icones sociaux dans le footer (sur toutes les pages). Vaux mieux rafraichir la page une fois rendu au footer pour voir l'animation.

 Animation 2: Animation par Keyframe sur le petit icône à 3 cercles faisant le tour de la page index. 3 fois. (Je n'ai pas réussi à faire en sorte de le faire disparaître à la fin de celle-ci.)

 Animation 3: Fenêtre popup newsletter, bouton d'accès au bas de la page index, au dessus du footer.